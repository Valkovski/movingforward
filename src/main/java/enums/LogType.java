package enums;

public enum LogType {
    INFO, ERROR, WARNING;
}
