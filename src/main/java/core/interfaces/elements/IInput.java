package core.interfaces.elements;

public interface IInput {
    public void write(String str);
}
